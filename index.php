<?php
include_once 'CalendarFeed.php';
require_once 'google-api/autoload.php';

$started = session_start();
if($started == FALSE) {
  error_log("failed to create or get session");
  exit(1);
}

$client_id = 'the-client-id-you-made.apps.googleusercontent.com'; 
$service_account_name = 'client-email@developer.gserviceaccount.com'; 
$key_file_location = 'path-to/the-client-key.p12'; 
$key = file_get_contents($key_file_location);

$redirect_uri = 'http://localhost/calendarFeed/';
$cred = new Google_Auth_AssertionCredentials(
    $service_account_name,
    array('https://www.googleapis.com/auth/calendar'),
    $key
);

$client = new Google_Client();
$client->setClientId($client_id);
$client->setApplicationName("Google Cal");

if (isset($_SESSION['service_token'])) {
  $client->setAccessToken($_SESSION['service_token']);
}


$client->setAssertionCredentials($cred);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion($cred);
}
$_SESSION['service_token'] = $client->getAccessToken();

$service = new Google_Service_Calendar($client);
$cf = new CalendarFeed($service);

header('Content-Type: application/json');
echo $cf->get_feeds(); 





