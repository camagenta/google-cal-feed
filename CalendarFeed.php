<?php 
class CalendarFeed {
  public $nodes;
  public $calendarFeeds;
  public $service;

  public function __construct($service){
    $this->nodes = array();
    $this->calendarFeeds = array(
     "Room Calendar A" => "email-for-rm-a@resource.calendar.google.com",
     "Room Calendar B" => "email-for-rmb@resource.calendar.google.com",
     // add more calendars... remmeber to grant the service account access    
      );        
    $this->service = $service;
  }
  
  public function get_feeds(){
    $currentDateTime = date("c");
    $params = array('timeMin'=>$currentDateTime,
                    'maxResults' => '20',
                    'singleEvents' => 'true', 
                    'orderBy'=>'startTime');

    foreach($this->calendarFeeds as $calKey=>$calValue){  
      $events = $this->service->events->listEvents($calValue, $params);
      foreach ($events->getItems() as $event) {
        $start = $event->getStart();
        $title = $event->getSummary();        
        $startTime = $start->getDateTime();
        $items = array(
          'title' => $title,
          'where' => $calKey,                  
          'when' => $startTime, 
        );
        array_push($this->nodes, $items);
      }
    }
    $JSonOut = json_encode($this->nodes);
    return $JSonOut;
  }
}
